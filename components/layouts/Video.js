import React from 'react';
import {
  Image,
  Text,
  View,
  Dimensions
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const width = Dimensions.get('window').width;
const imageWidth = width - 2*20;
class Video extends React.Component {
  render() {
    return (
      <View
        style={{
          padding: 20,
          borderBottomColor: '#D3D3D3',
          borderBottomWidth: 1,
        }}
      >
        <Image source={require('./400x200.png')}
          style={{
            width:imageWidth,
          }}
        />
        <View style={{
          flexDirection: 'row',
          paddingTop: 20
        }}>
          <Image source={require('./robot-dev.png')}
            style={{
              height: 50,
              width: 50,
              borderRadius: 25,
              resizeMode: 'cover'
            }}
          />
          <View style={{
            flex: 1,
            paddingLeft: 10
          }}>
            <Text style={{ flex: 1, flexWrap: 'wrap' }}>{this.props.video.nameVideo}</Text>
            <View style={{
              flexDirection: 'row',
            }}>
              <Text style={{}}>{this.props.video.user}</Text>
              <FontAwesome name='circle' color='#B0C4DE' size={5} style={{
                alignItems: 'center',
                padding: 10
              }} />
              <Text style={{ flex: 1, flexWrap: 'wrap' }}>{this.props.video.seeCount} lượt xem</Text>
            </View>
            <Text style={{}}>{this.props.video.time}</Text>
          </View>
          <View style={{
            paddingTop: 10
          }}>
            <FontAwesome name='ellipsis-v' size={20} color='#B0C4DE' />
          </View>
        </View>
      </View >
    );
  }
}
export default Video;