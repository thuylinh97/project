import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

class Header extends React.Component {
  render() {
    return (
      <View
        style={{
          height: 60,
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: 'white',
          padding: 16,
          paddingTop: 30,
          paddingLeft: 20,
          shadowColor: 'white',
          shadowOpacity: 0.2,
          shadowOffset: {
            width: 0,
            height: 5
          }
        }}
      >
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <FontAwesome size={30} name='youtube-play' color='red' />
          <Text style={{ fontSize: 20 }}>Youtube</Text>
        </View>
        <FontAwesome size={20} name='video-camera' style={{ marginLeft: 10, marginRight: 10 }} />
        <FontAwesome size={20} name='search' style={{ marginLeft: 10, marginRight: 10 }} />
        <View
          style={{
            width: 30,
            backgroundColor: 'green',
            height: 30,
            borderRadius: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: 'white' }}>{this.props.userName}</Text>
        </View>
      </View>
    );
  }
}
export default Header;